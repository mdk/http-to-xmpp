# HTTP to XMPP gateway

This is a dumb dumb gateay between HTTP and XMPP:

You POST some text over HTTP, text gets sent over XMPP.

Only one XMPP jid can receive message.


## Installation

    pip install http-to-xmpp


## Setup

It can be configured either via environment variables:

```bash
$ export XMPP_DEST_JID=the_human_receiving_messages@the_server.org
$ export XMPP_JID=the_bot_account@the_server.org
$ export XMPP_PASSWORD=the_bot_password
$ http-to-xmpp
```

or via arguments:

```bash
$ http-to-xmpp --xmpp-jid bot_account@the_server.org --xmpp-password "$(pass bot_account@the_server.org)" --xmpp-dest-jid human_account@the_server.org
```

HTTP host and port to listen to can be changed using `--http-host` and `--http-port`, they default to `localhost:1985`.


## Usage

You just have to send POST requests, to `/` on the given host:port
pair so by default, using curl, one can post messages using:

    $ curl -XPOST -d Coucou localhost:1985
